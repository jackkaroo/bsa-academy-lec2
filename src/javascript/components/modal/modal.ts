import { createElement } from '../../helpers/domHelper';
import { IWinner, ICreateElement } from '../../helpers/interfaces';


export function showModal(winner:IWinner) {
  const root = getModalContainer();
  const modal = createModal(winner); 
  
  if (root!==null) root.append(modal);
}

function getModalContainer() {
  return document.getElementById('root');
}

function createModal(winner:IWinner) {
  const layer = createElement({ tagName: 'div', className: 'modal-layer' });
  const modalContainer = createElement({ tagName: 'div', className: 'modal-root' });
  const header = createHeader(winner.title);
  
  modalContainer.append(header, winner.bodyElement);
  layer.append(modalContainer);

  return layer;
}

function createHeader(title:string) {
  const headerElement = createElement({ tagName: 'div', className: 'modal-header' });
  const titleElement = createElement({ tagName: 'span',className:'' });
  const closeButton = createElement({ tagName: 'div', className: 'close-btn' });
  
  
  titleElement.innerText = title;
  closeButton.innerText = '×';
  
  const close = () => {
    hideModal();
  }
  closeButton.addEventListener('click', close);
  headerElement.append(title, closeButton);
  
  return headerElement;
}

function hideModal() {
  const modal = document.getElementsByClassName('modal-layer')[0];
  modal?.remove();
}
