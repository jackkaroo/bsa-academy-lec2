import { showModal } from './modal'
import { IFighterDetails, IWinner } from '../../helpers/interfaces'

export function showWinnerModal(fighter:IFighterDetails) {
  // call showModal function 
  let winner:IWinner = {
    title:`${fighter.name} win!`,
    bodyElement:`Health:${fighter.health}, Attack:${fighter.attack}, Defense:${fighter.defense}`
  }
  showModal(winner)
}
