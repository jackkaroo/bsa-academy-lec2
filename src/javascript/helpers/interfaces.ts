export interface IFighter {
  _id: string,
  name: string,
  source: string
}

export interface IFighterDetails {
  _id: string,
  name: string,
  health: number,
  attack: number,
  defense: number,
  source: string,
}

export interface IWinner {
  title:string,
  bodyElement:string
}

export interface ICreateElement {
  tagName:string,
  className?:string,
  attributes?:Object
}